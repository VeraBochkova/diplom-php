<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class SeedCategories extends Migration
{
    private function getCategoriesArray()
    {
        $category = [];
        $category[] = [
            'id' => 1,
            'name' => 'Категория 1'
        ];
        $category[] = [
            'id' => 2,
            'name' => 'Категория 2'
        ];
        $category[] = [
            'id' => 3,
            'name' => 'Категория 3'
        ];
        $category[] = [
            'id' => 4,
            'name' => 'Категория 4'
        ];
        $category[] = [
            'id' => 5,
            'name' => 'Категория 5'
        ];
        $category[] = [
            'id' => 6,
            'name' => 'Категория 6'
        ];
        return $category;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function(){
            DB::table('categories')->delete();
            DB::table('categories')->insert(
                $this->getCategoriesArray()
            );
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}