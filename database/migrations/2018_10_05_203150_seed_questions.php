<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
class SeedQuestions extends Migration
{
    private function getQuestionsArray()
    {
        $question = [];
        $now = Carbon::now()->toDateTimeString();
        $question[] = [
            'id' => 1,
            'question' => 'Вопрос 1',
            'category_id' => 1,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 2,
            'question' => 'Вопрос 2',
            'category_id' => 1,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 3,
            'question' => 'Вопрос 3',
            'category_id' => 1,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 4,
            'question' => 'Вопрос 4',
            'category_id' => 1,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 5,
            'question' => 'Вопрос 5',
            'category_id' => 2,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 6,
            'question' => 'Вопрос 6',
            'category_id' => 2,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 7,
            'question' => 'Вопрос 7',
            'category_id' => 2,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 8,
            'question' => 'Вопрос 8',
            'category_id' => 3,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 9,
            'question' => 'Вопрос 9',
            'category_id' => 3,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 10,
            'question' => 'Вопрос 10',
            'category_id' => 3,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 11,
            'question' => 'Вопрос 11',
            'category_id' => 3,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 12,
            'question' => 'Вопрос 12',
            'category_id' => 4,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 13,
            'question' => 'Вопрос 13',
            'category_id' => 4,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 14,
            'question' => 'Вопрос 14',
            'category_id' => 4,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 15,
            'question' => 'Вопрос 15',
            'category_id' => 5,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 16,
            'question' => 'Вопрос 16',
            'category_id' => 5,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 17,
            'question' => 'Вопрос 17',
            'category_id' => 5,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 18,
            'question' => 'Вопрос 18',
            'category_id' => 5,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 19,
            'question' => 'Вопрос 19',
            'category_id' => 6,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 20,
            'question' => 'Вопрос 20',
            'category_id' => 6,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 21,
            'question' => 'Вопрос 21',
            'category_id' => 6,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 22,
            'question' => 'Вопрос 22',
            'category_id' => 6,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 23,
            'question' => 'Вопрос 23',
            'category_id' => 6,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 24,
            'question' => 'Вопрос 24',
            'category_id' => 6,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 25,
            'question' => 'Вопрос 25',
            'category_id' => 6,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 26,
            'question' => 'Вопрос 26',
            'category_id' => 6,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $question[] = [
            'id' => 27,
            'question' => 'Вопрос 27',
            'category_id' => 6,
            'author' => 'Anonymous',
            'published' => true,
            'created_at' => $now,
            'updated_at' => $now
        ];
        return $question;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function(){
            DB::table('questions')->delete();
            DB::table('questions')->insert(
                $this->getQuestionsArray()
            );
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}